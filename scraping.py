import json
import pandas as pd
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from pymongo import MongoClient

class scrapeWeb():
    def __init__(self):
        with open('reference.json') as f:
            self.ref = json.load(f)

        self.chromeOptions = Options()
        self.chromeOptions.add_argument('--headless')
        # self.chromeOptions.add_argument('--disable-gpu')

        self.driver = webdriver.Chrome(options=self.chromeOptions)

        self.outputPath = 'output/'
        self.mongoDatabaseName = 'blogs_db'

    def insertToMongo(self, databaseName, collectionName, df):
        mongoUri = "mongodb://localhost:27017/"

        client = MongoClient(mongoUri)

        db = client[databaseName]
        collection = db[collectionName]

        dfRenamed = df.rename(columns = {
            'Title': 'title',
            'Link URL': 'link_url',
            'Publication Date': 'publication_date',
            'Author': 'author',
            'Page Content': 'page_content'
        })

        data = dfRenamed.to_dict(orient='records')

        collection.insert_many(data)

        client.close()

    def scrapeCloudflare(self):
        webTitle = 'cloudflare'

        ref = self.ref[webTitle]

        self.driver.get(ref['webUrl']) # Access home page

        pages = self.driver.find_elements(By.XPATH, '//div[@class="' + ref['pagination'] + '"]/div/div/ul/li/a') # Get pagination values
           
        pagesToCrawl = [page.get_attribute('href') for page in pages]
        indexPages = [int(idxPage.replace(ref['webUrl'], '').replace('page/', '')) for idxPage in pagesToCrawl if idxPage != ref['webUrl']]

        listTagArticles = self.driver.find_elements(By.XPATH, "//article/div/a[@data-testid='post-title']") # Get tag refer to articles
        listUrlArticles = list(set([article.get_attribute('href') for article in listTagArticles])) # Get article url

        # Enable this part if you want to crawl all articles withitn website
        # for idx in range(indexPages[0], indexPages[1]+1):
        #     self.driver.get(ref['webUrl'] + 'page/' + str(idx))

        #     listTagArticlesEachPage = self.driver.find_elements(By.XPATH, "//article/div/a[@data-testid='post-title']")
        #     listUrlArticlesEachPage = [article.get_attribute('href') for article in listTagArticlesEachPage]
        #     listUrlArticles.extend(listUrlArticlesEachPage)

        # print(len(listUrlArticles))

        articles_data = []

        for url in listUrlArticles:
            self.driver.get(url)
            title = self.driver.find_element(By.XPATH, ref['title']).text.strip()
            publicationDate = datetime.strptime(self.driver.find_element(By.XPATH, ref['publicationDate']).text.strip(), "%m/%d/%Y")
            publicationDateIsoFormat = datetime.strftime(publicationDate, "%Y-%m-%d")
            listAuthors = self.driver.find_elements(By.XPATH, ref['authors'])
            authors = ", ".join([a.text.strip() for a in listAuthors])
            fullContent = self.driver.find_elements(By.XPATH, ref['content'])
            content = '\n'.join([c.text.strip() for c in fullContent])

            article_data = {
                'Title': title,
                'Link URL': url,
                'Publication Date': publicationDateIsoFormat,
                'Author': authors,
                'Page Content': content
            }
            
            articles_data.append(article_data)

        df = pd.DataFrame.from_dict(articles_data) # Convert the result to pandas

        df.to_csv(self.outputPath + webTitle + '.csv', index = False) # Save as csv file

        self.insertToMongo(self.mongoDatabaseName, webTitle, df)

    def scrapeDanielMiessler(self):
        webTitle = 'danielmiessler'

        ref = self.ref[webTitle]

        self.driver.get(ref['webUrl']) # Access home page

        listTagArticles = self.driver.find_elements(By.XPATH, "//a") # Get tag refer to articles
        listUrlArticles = list(set([article.get_attribute('href') for article in listTagArticles if article.get_attribute('href').startswith('https://danielmiessler.com/p/')])) # Get article url

        articles_data = []

        for url in listUrlArticles:
            self.driver.get(url)
            title = self.driver.find_element(By.XPATH, ref['title']).text.strip()
            authorsAndPublicationDate = self.driver.find_element(By.XPATH, ref['publicationDate'])
            authors = authorsAndPublicationDate.find_element(By.XPATH, '//span/a').text.strip()
            publicationDate = datetime.strptime(authorsAndPublicationDate.find_element(By.XPATH, "//span[@class='text-wt-text-on-background']").text.strip(), "%B %d, %Y")
            publicationDateIsoFormat = datetime.strftime(publicationDate, "%Y-%m-%d")
            fullContent = self.driver.find_element(By.XPATH, ref['content']).find_elements(By.XPATH, "//p")
            content = "\n".join([c.text.strip() for c in fullContent])
            
            article_data = {
                'Title': title,
                'Link URL': url,
                'Publication Date': publicationDateIsoFormat,
                'Author': authors,
                'Page Content': content
            }
            
            articles_data.append(article_data)

        df = pd.DataFrame.from_dict(articles_data) # Convert the result to pandas

        df.to_csv(self.outputPath + webTitle + '.csv', index = False) # Save as csv file

        self.insertToMongo(self.mongoDatabaseName, webTitle, df)
    
    def scrapeOwasp(self):
        webTitle = 'owasp'

        ref = self.ref[webTitle]

        self.driver.get(ref['webUrl']) # Access home page

        listTagArticles = self.driver.find_elements(By.XPATH, "//section[@class='homepage-blog']/h2/a") # Get tag refer to articles
        listUrlArticles = list(set([article.get_attribute('href') for article in listTagArticles  if article.get_attribute('href').startswith('https://owasp.org/blog/')])) # Get article url

        articles_data = []

        for url in listUrlArticles:
            self.driver.get(url)
            title = self.driver.find_element(By.XPATH, ref['title']).text.strip()
            authorAndPublicationDate = self.driver.find_element(By.XPATH, ref['publicationDate'])
            authors = authorAndPublicationDate.find_element(By.XPATH, ref['authors']).text.strip()
            tagP = authorAndPublicationDate.find_elements(By.XPATH, ref['publicationDate'] + '//p')
            publicationDate = datetime.strptime([p.text.strip() for p in tagP][1], "%A, %B %d, %Y")
            publicationDateIsoFormat = datetime.strftime(publicationDate, "%Y-%m-%d")
            fullContent = authorAndPublicationDate.find_elements(By.XPATH, ref['publicationDate'] + '//p|//h2')
            content = "\n".join([c.text.strip() for c in fullContent])

            article_data = {
                'Title': title,
                'Link URL': url,
                'Publication Date': publicationDateIsoFormat,
                'Author': authors,
                'Page Content': content
            }
            
            articles_data.append(article_data)

        df = pd.DataFrame.from_dict(articles_data) # Convert the result to pandas

        df.to_csv(self.outputPath + webTitle + '.csv', index = False) # Save as csv file

        self.insertToMongo(self.mongoDatabaseName, webTitle, df)

    def runScraping(self):
        self.scrapeCloudflare()
        self.scrapeDanielMiessler()
        self.scrapeOwasp()

if __name__ == '__main__':
    run = scrapeWeb()
    run.runScraping()